#include "rotation.h"
#include <stdlib.h>

struct image *rotate(struct image* const src) {
    struct image *dst = (struct image*) malloc(sizeof(struct image));
    dst->width = src->height;
    dst->height = src->width;
    dst->data = (struct pixel*) malloc(sizeof(struct pixel) * dst->width * dst->height);
    for (size_t i = 0; i < dst->height; i++) {
        for (size_t j = 0; j < dst->width; j++) {
            dst->data[i * dst->width + j] = src->data[(src->height - j - 1) * src->width + i];
        }
    }
    return dst;
}
