#ifndef ROTATE_H
#define ROTATE_H

#include "../image/image.h"
#include "../utilities/statuses.h"

struct image *rotate(struct image* const img);

#endif
