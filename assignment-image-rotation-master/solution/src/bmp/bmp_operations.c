#include "bmp_operations.h"
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;

};
#pragma pack(pop)

static enum operation_status read_header(FILE* const f, struct bmp_header* const header) {

    if (fread(header, sizeof(struct bmp_header), 1, f) != 1)
        return READ_ERROR;
    
    return SUCCESS;
    
}
static uint8_t get_padding(const uint32_t width) {
    const uint8_t padding = (width * sizeof(struct pixel)) % 4;
    return padding ? 4 - padding : 0;
}

static enum operation_status read_pixels(FILE* const f, struct image* const img) {

    const uint32_t width = img->width;
    const uint32_t height = img->height;
    
    const uint8_t padding = get_padding(width);
    
    for (size_t i = 0; i < height; ++i) {
        if (fread(img->data + i * width, sizeof(struct pixel), width, f ) != width)
            return READ_ERROR;

        if (fseek(f, padding, SEEK_CUR) != 0)
            return SEEK_ERROR;
    }
    return SUCCESS;
}


static enum operation_status read_image(FILE* const f, const struct bmp_header* const header, struct image* const img) {

    *img = new_image(header->biWidth, header-> biHeight);

    if (img->data == NULL) {
        free_image_memory(img);
        return ALLOCATION_ERROR;
    }

    if (fseek(f, header->bOffBits, SEEK_SET) != 0) {
        free_image_memory(img);
        return SEEK_ERROR;
    }

    return read_pixels(f, img);
}
static const struct bmp_header new_header = {
    .bfType = 0x4D42,
    .bfReserved = 0,
    .bOffBits = sizeof(struct bmp_header),
    .biSize = 40,
    .biPlanes = 1,
    .biBitCount = 24,
    .biCompression = 0,
    .biXPelsPerMeter = 2834,
    .biYPelsPerMeter = 2834,
    .biClrUsed = 0,
    .biClrImportant = 0
};

static uint32_t calculate_image_size(const uint64_t width, const uint64_t height) {
    const uint8_t padding = get_padding(width);
    return (width * sizeof(struct pixel) + padding) * height;
}


static uint32_t calculate_file_size(const uint32_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

static struct bmp_header create_header(const struct image* const img) {
    struct bmp_header header = new_header;
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    header.biWidth = width;
    header.biHeight = height;

    header.biSize = calculate_image_size(width, height);
    header.bfileSize = calculate_file_size(header.biSizeImage);

    return header;
} 

static enum operation_status write_header(FILE* f, const struct bmp_header* const header) {
    if (fseek(f, 0, SEEK_SET) != 0)
        return SEEK_ERROR;
    if (fwrite(header, sizeof(struct bmp_header), 1, f) != 1)
        return WRITE_ERROR;
    return SUCCESS;
}

static enum operation_status write_image(FILE* const f, const struct image* const img) {
    if (fseek(f, sizeof(struct bmp_header), SEEK_SET) != 0)
        return SEEK_ERROR;

    const uint8_t padding = get_padding(img->width);
    
    for (size_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, f) != img->width)
            return WRITE_ERROR;
        if (fseek(f, padding, SEEK_CUR) != 0)
            return SEEK_ERROR;
    }
    return SUCCESS;
}

enum operation_status to_bmp(FILE* const f, const struct image* const img) {
    const struct bmp_header header = create_header(img);
    enum operation_status status = write_header(f, &header);
    if (status != SUCCESS)
        return status;
    return write_image(f, img);}

enum operation_status from_bmp(FILE* const f, struct image* const img) {
    struct bmp_header header = {0};

    enum operation_status status = read_header(f, &header);
    if (status != SUCCESS) return status;

    return read_image(f, &header, img);
}
