#include "image.h"
#include <stdio.h>
#include <stdlib.h>


struct image new_image(const uint32_t width, const uint32_t height) {
  struct image img = {0};
  img.width = width;
  img.height = height;
  img.data = malloc(width * height * sizeof(struct pixel));
  return img;
}

void free_image_memory(struct image *img) {
  free(img->data);
  free(img);
}

