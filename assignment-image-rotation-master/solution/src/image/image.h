#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include "image.h"

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image new_image(const uint32_t width, const uint32_t height);

void free_image_memory(struct image *img);



#endif
