#include "file_operations/work_with_file.h"
#include "rotate/rotation.h"
#include <stdio.h>
#include <stdlib.h>

void handle_error(enum operation_status status, struct image *img);

int main(int argc, char** argv) {

    if (argc != 3) {
        printf("Необходимо ввести 2 аргумента \n");
        return 0;
    }

    const char* const in_filename = argv[1];
    const char* const out_filename = argv[2];

    struct image *src = (struct image*) malloc(sizeof(struct image));

    enum operation_status status = work_with_file(in_filename, src);

    if (status != SUCCESS) {
        handle_error(status, src);
    }

    struct image *dst = rotate(src);
    free_image_memory(src);

    status = write_bmp_file(out_filename, dst);
    if (status != SUCCESS) {
        handle_error(status, dst);
    }



    free_image_memory(dst);
    printf("Картинка была успешно повернута\n");

    return 0;
}

