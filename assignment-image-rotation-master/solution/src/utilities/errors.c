#include "../image/image.h"
#include "statuses.h"
#include "stdio.h"
#include <stdlib.h>

static char* error_messages[] = {
    [SUCCESS] = "Успешно",
    [READ_ERROR] = "Ошибка при чтении файла",
    [OPEN_ERROR] = "Не удается открыть файл",
    [SEEK_ERROR] = "Ошибка во время работы с файлом",
    [WRITE_ERROR] = "Ошибка чтения файла",
    [CLOSE_ERROR] = "Не удается закрыть файл",
    [ALLOCATION_ERROR] = "Невозможно выделить память"
};

void handle_error(const enum operation_status status, struct image *img) {
    free_image_memory(img);
    fprintf(stderr, "%s\n", error_messages[status]);
    abort();
}
