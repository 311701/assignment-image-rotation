#include "../bmp/bmp_operations.h"
#include "work_with_file.h"


static enum operation_status open_file(FILE** const  f, const char* const filename, const char* const mode) {
    *f = fopen(filename, mode);
    if (*f == NULL) {
        return OPEN_ERROR;
    }
    return SUCCESS;
}


static enum operation_status close_file(FILE* f) {
    const int result = fclose(f);
    if (result != 0) {
        return CLOSE_ERROR;
    }
    return SUCCESS;
}



enum operation_status work_with_file(const char* const file_name, struct image* const img) {
    FILE* f = NULL;
    enum operation_status status = open_file(&f, file_name, "rb");
    if (status != SUCCESS)
        return status;

    status = from_bmp(f, img);
    if (status != SUCCESS){
        close_file(f);
        return status;
    }
        
    
    return close_file(f);
}

enum operation_status write_bmp_file(const char* const file_name, struct image* const img) {
    FILE* f = NULL;
    enum operation_status status = open_file(&f, file_name, "wb");
    if (status != SUCCESS)
        return status;

    status = to_bmp(f, img);
    if (status != SUCCESS){
        close_file(f);
        return status;
    }
        

    return close_file(f);
}

