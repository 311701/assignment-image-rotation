#ifndef FILE_WORK_H
#define FILE_WORK_H

#include "../image/image.h"
#include "../utilities/statuses.h"

//enum operation_status work_with_file(const char* const file_name, struct image* const img);

enum operation_status work_with_file(const char* const file_name, struct image* const img);
enum operation_status write_bmp_file(const char* const file_name, struct image* const img);

#endif
