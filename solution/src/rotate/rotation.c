#include "../image/image.h"
#include "../utilities/statuses.h"
//#include "image.h"


#include <inttypes.h>
#include  <stdint.h>
#include <string.h>


static struct pixel get_pixel_by_coordinates(const struct image* const img, const uint32_t x, const uint32_t y) {
    return img->data[x * img->width + y];
}

static void set_pixel_by_coordinates(struct image* const img, const uint32_t x, const uint32_t y, const struct pixel pixel) {
    img->data[x * img->width + y] = pixel;
}


enum operation_status rotate(struct image* const img) {
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    
    struct image new_img = new_image(height, width);


    if (new_img.data == NULL) return ALLOCATION_ERROR;

    for (uint32_t i = 0; i < height; ++i) {
        for(uint32_t j = 0; j < width; ++j) {
            const struct pixel pixel = get_pixel_by_coordinates(img, i, j);
            set_pixel_by_coordinates(&new_img, j, height - i - 1, pixel);
        }
    }

    free_image_memory(*img);
    *img = new_img;

    return SUCCESS;

}
