#ifndef ROTATE_H
#define ROTATE_H

#include "../image/image.h"
#include "../utilities/statuses.h"

enum operation_status rotate(struct image* const img);

#endif
