#ifndef READ_WRITE_BMP_H
#define READ_WRITE_BMP_H 

#include "../image/image.h"
#include "../utilities/statuses.h"
#include "bmp.h"
#include <stdio.h>

enum operation_status from_bmp(FILE* const f, struct image* const img);
enum operation_status to_bmp(FILE* const f, const struct image* const img);

#endif
